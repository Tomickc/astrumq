package com.tmckdevelopment.astrumq.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tmckdevelopment.astrumq.R;
import com.tmckdevelopment.astrumq.model.Author;
import com.tmckdevelopment.astrumq.model.Message;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tomáš Vítek on 06.04.2016 at 11:37.
 */

//Custom adapter with my own layout
public class MessagesAdapter extends ArrayAdapter<Message> {

    private final Activity context;
    private final List<Author> authors;
    private final List<Message> messages;

    public MessagesAdapter(Activity context,List<Author> authors, List<Message> messages) {
        super(context, R.layout.item_message, messages);

        this.context = context;
        this.authors = authors;
        this.messages = messages;
    }

    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.item_message, null, true);


        TextView email = (TextView)rowView.findViewById(R.id.textViewEmailItem);
        TextView message = (TextView)rowView.findViewById(R.id.textViewMessageText);
        TextView date = (TextView)rowView.findViewById(R.id.textViewDate);

        email.setText(authors.get(position).email);
        message.setText(messages.get(position).title+"\n"+messages.get(position).description);
        date.setText(messages.get(position).dateCreated);

        return rowView;
    }
}