package com.tmckdevelopment.astrumq;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.tmckdevelopment.astrumq.fragment.BaseFragment;
import com.tmckdevelopment.astrumq.fragment.SplashscreenFragment;
import com.tmckdevelopment.astrumq.model.Author;
import com.tmckdevelopment.astrumq.model.Message;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Declare objects
    Toolbar toolbar;
    public Author author;
    public List<Author> authors;
    public List<Message> messages;


    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Find views by their ID
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        //Set app's support actionBar
        setSupportActionBar(toolbar);

        //show splashscreen
        showFragment(SplashscreenFragment.newInstance(), SplashscreenFragment.TAG, false);
    }

    private void showFragment(BaseFragment fragment, String tag, boolean backStack) {

        //Define transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //Replace fragment
        transaction.replace(R.id.fragment, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
