package com.tmckdevelopment.astrumq.model;

/**
 * Created by Tomáš Vítek on 13.05.2016 at 22:52.
 */
public class Message {

    public int id;
    public String dateCreated,title,description;

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
