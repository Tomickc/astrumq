package com.tmckdevelopment.astrumq.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.tmckdevelopment.astrumq.MainActivity;
import com.tmckdevelopment.astrumq.R;
import com.tmckdevelopment.astrumq.model.User;
import com.tmckdevelopment.astrumq.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tomáš Vítek on 12.05.2016 at 19:06.
 */
public class RegisterFragment extends BaseFragment implements Validator.ValidationListener {

    //Set fragment's TAG
    public static final String TAG = "register_fragment";

    private int PICK_IMAGE_REQUEST = 1;

    //define objects and variables
    User user;
    Validator validator;
    CircleImageView profileImage;
    Button registerButton;
    View view;

    //Views with validation
    @NotEmpty
    EditText nameEditText, surnameEditText;

    @NotEmpty
    @Email
    EditText emailEditText;

    @NotEmpty
    @Password
    EditText passwordEditText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        view = inflater.inflate(R.layout.fragment_register, container, false);

        //declare views
        profileImage = (CircleImageView) view.findViewById(R.id.profileImageRegistration);
        nameEditText = (EditText) view.findViewById(R.id.editTextNameRegistration);
        surnameEditText = (EditText) view.findViewById(R.id.editTextSurnameRegistration);
        emailEditText = (EditText) view.findViewById(R.id.editTextEmailRegistration);
        passwordEditText = (EditText) view.findViewById(R.id.editTextPasswordRegistration);
        registerButton = (Button) view.findViewById(R.id.buttonRegisterRegistration);

        //Set toolbar title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Registrace");

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back when arrow is clicked
                getFragmentManager().popBackStack();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //validator will check errors
        validator = new Validator(this);
        validator.setValidationListener(this);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check  editTexts
                validator.validate();
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Vyber aplikaci"), PICK_IMAGE_REQUEST);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                //Chceck if image is not too large
                if (bitmap.getWidth() <= 2000 && bitmap.getHeight() <= 2000) {
                    profileImage.setImageBitmap(bitmap);
                } else
                    Toast.makeText(getActivity(), "Soubor je moc velký", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Fragment's newInstance method
    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }

    @Override
    public void onValidationSucceeded() {
        //get datas from edittexts

        //Put datas to parameters
        RequestParams params = new RequestParams();
        params.put("email", emailEditText.getText().toString());
        params.put("password", passwordEditText.getText().toString());
        params.put("name", nameEditText.getText().toString());
        params.put("surname", surnameEditText.getText().toString());

        MyHttpClient.post("login", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

                Snackbar.make(view, "Chyba", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Snackbar.make(view, "Registrace byla úspěšná", Snackbar.LENGTH_SHORT).show();

                Gson gson = new Gson();

                //Save values from JSON to user.class
                user = gson.fromJson(response.toString(), User.class);

                //Save new user's email and password to sharedPrefs
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("email", emailEditText.getText().toString());
                editor.putString("password", passwordEditText.getText().toString());
                editor.apply();

                //Go to LoginFragment
                getFragmentManager().popBackStack();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
