package com.tmckdevelopment.astrumq.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tmckdevelopment.astrumq.MainActivity;
import com.tmckdevelopment.astrumq.R;
import com.tmckdevelopment.astrumq.adapter.MessagesAdapter;
import com.tmckdevelopment.astrumq.model.Author;
import com.tmckdevelopment.astrumq.model.Message;
import com.tmckdevelopment.astrumq.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 13.05.2016 at 20:39.
 */
public class MessagesFragment extends BaseFragment implements Validator.ValidationListener {

    //Set fragment's TAG
    public static final String TAG = "messages_fragment";

    //Define objects
    ListView messagesList;
    List<Message> messages;
    List<Author> authors;
    Validator validator;
    ImageView post;
    MessagesAdapter messagesAdapter;

    boolean firstStart = true;

    @NotEmpty
    EditText messageEditText, titleEditText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable icons in toolbar
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        //Set toolbar's title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Konverzace");

        //Declare views
        messagesList = (ListView) view.findViewById(R.id.listViewMessages);
        post = (ImageView) view.findViewById(R.id.imageViewPost);
        messageEditText = (EditText) view.findViewById(R.id.editTextPostMessage);
        titleEditText = (EditText) view.findViewById(R.id.editTextTitle);

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);


        //Create navigation drawer
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(getActivity())
                .withHeaderBackgroundScaleType(ImageView.ScaleType.CENTER_CROP)
                .withHeaderBackground(R.drawable.background)
                .addProfiles(
                        new ProfileDrawerItem().withEmail(sharedPreferences.getString("email", "")).withIcon(getResources().getDrawable(R.drawable.avatar))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        //Now create your drawer and pass the AccountHeader.Result
        drawerBuilder = new DrawerBuilder()

                .withAccountHeader(headerResult)
                .withActivity(getActivity())
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Odhlásit se")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        //Get back to loginFragment
                        showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
                        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        drawerBuilder.removeAllItems();
                        drawerBuilder.removeHeader();
                        drawerBuilder.removeAllStickyFooterItems();

                        return false;
                    }
                })
                .withToolbar(((MainActivity) getActivity()).getToolbar())
                .buildForFragment();

        messages = new ArrayList<>();
        authors = new ArrayList<>();

        //validator will check errors
        validator = new Validator(this);
        validator.setValidationListener(this);

        //Create parameters
        RequestParams requestParams = new RequestParams();
        requestParams.put("limitFilter", 10);
        requestParams.put("offsetFilter", 0);

        //If app is first time started load all messages and save it
        if (firstStart) {
            MyHttpClient.get("messages?offsetFilter=0&limitFilter=10", requestParams, new JsonHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    //Treat with try/catch
                    try {

                        ///create new instance of GSON
                        Gson gson = new Gson();

                        //Array of Messages
                        JSONArray messagesArray = response.getJSONArray("messages");


                        //Get all Messages
                        for (int i = 0; i < messagesArray.length(); i++) {

                            //get current Message
                            Message message = gson.fromJson(messagesArray.getJSONObject(i).toString(), Message.class);
                            //get current Author
                            Author author = gson.fromJson(messagesArray.getJSONObject(i).getJSONObject("author").toString(), Author.class);

                            //add Author to List
                            authors.add(author);
                            //add Message to List
                            messages.add(message);
                        }

                        //Show list of messages
                        messagesAdapter = new MessagesAdapter(getActivity(), authors, messages);
                        messagesList.setAdapter(messagesAdapter);


                        ((MainActivity) getActivity()).setAuthors(authors);
                        ((MainActivity) getActivity()).setMessages(messages);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                }
            });
            firstStart = false;

        } else {
            //Show list of saved messages
            MessagesAdapter messagesAdapter = new MessagesAdapter(getActivity(), ((MainActivity) getActivity()).authors, ((MainActivity) getActivity()).messages);
            messagesList.setAdapter(messagesAdapter);
        }
        messagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Show detail
                showFragment(MessageDetailFragment.newInstance(), MessageDetailFragment.TAG, true);
                ((MainActivity) getActivity()).setAuthor(((MainActivity) getActivity()).authors.get(position));
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Start validation
                validator.validate();

            }
        });

    }

    //Fragment's newInstance method
    public static MessagesFragment newInstance() {
        MessagesFragment fragment = new MessagesFragment();
        return fragment;
    }

    @Override
    public void onValidationSucceeded() {


        //Put datas to parameters
        //Create parameters
        RequestParams requestParams = new RequestParams();
        requestParams.put("limitFilter", 10);
        requestParams.put("offsetFilter", 0);
        requestParams.put("title", titleEditText.getText().toString());
        requestParams.put("description", messageEditText.getText().toString());

        MyHttpClient.post("messages?limitFilter=10&offsetFilter=0", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Calendar c = Calendar.getInstance();

                //get current time
                SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");

                //save datas from JSON to Message.class
                Message message = new Message();
                message.setTitle(titleEditText.getText().toString());
                message.setDescription(messageEditText.getText().toString());
                message.setDateCreated(dateformat.format(c.getTime()));

                Author author = new Author();
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

                author.setEmail(sharedPreferences.getString("email", ""));

                //add Author to List
                authors.add(author);
                //add Message to List
                messages.add(message);

                //Show list of messages
                MessagesAdapter messagesAdapter = new MessagesAdapter(getActivity(), authors, messages);
                messagesList.setAdapter(messagesAdapter);


                messageEditText.setText("");
                titleEditText.setText("");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    //Show add icon in toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_messages, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add icon click
        if (id == R.id.settings) {
            //checkBox text
            final CharSequence[] items = {"Podle data", "Abecedně"};

            //Create new dialog with checkBox
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Filtr")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {


                            switch (indexSelected) {
                                case 0:

                                    break;
                                case 1:

                                    break;
                            }

                        }
                    })
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.dismiss();
                        }
                    }).setNegativeButton("Konec", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //dismiss dialog
                            dialog.dismiss();
                        }
                    }).create();

            //Show dialog
            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
