package com.tmckdevelopment.astrumq.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.mikepenz.materialdrawer.Drawer;
import com.tmckdevelopment.astrumq.R;

/**
 * Created by Tomáš Vítek on 12.05.2016 at 19:06.
 */

//All fragments will extend from BaseFragment
public class BaseFragment extends Fragment {

    public Drawer drawerBuilder;

    //show new fragment method
    public void showFragment(BaseFragment fragment, String tag, boolean backStack) {

        //create new transaction
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        //transaction will start new fragment
        transaction.replace(R.id.fragment, fragment, tag);

        //check if transaction has backstack
        if (backStack) {

            //backstack available
            transaction.addToBackStack(tag);
        }

        //commit transaction
        transaction.commit();
    }
}
