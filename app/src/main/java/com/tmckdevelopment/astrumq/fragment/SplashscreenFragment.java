package com.tmckdevelopment.astrumq.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tmckdevelopment.astrumq.MainActivity;
import com.tmckdevelopment.astrumq.R;

/**
 * Created by Tomáš Vítek on 12.05.2016 at 19:06.
 */
public class SplashscreenFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "splashscreen_fragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splashscreen, container, false);

        //Set actionBar text as nothing
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //afer 2 sec show loginFragment
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
            }
        }, 2000);

    }

    //Fragment's newInstance method
    public static SplashscreenFragment newInstance() {
        SplashscreenFragment fragment = new SplashscreenFragment();
        return fragment;
    }

}
