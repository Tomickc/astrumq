package com.tmckdevelopment.astrumq.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.tmckdevelopment.astrumq.MainActivity;
import com.tmckdevelopment.astrumq.R;
import com.tmckdevelopment.astrumq.model.User;
import com.tmckdevelopment.astrumq.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 12.05.2016 at 19:06.
 */
public class LoginFragment extends BaseFragment implements Validator.ValidationListener {

    //Set fragment's TAG
    public static final String TAG = "login_fragment";

    //define objects and variables
    Button login, register;
    CheckBox stayLogged;
    User user;
    View view;
    Validator validator;
    SharedPreferences sharedPreferences;

    String emailString, passwordString;

    //define objects with validation
    @NotEmpty
    @Email
    EditText email;

    @NotEmpty
    @Password
    EditText password;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        //declare views
        login = (Button) view.findViewById(R.id.buttonLogin);
        register = (Button) view.findViewById(R.id.buttonRegister);
        stayLogged = (CheckBox) view.findViewById(R.id.checkBoxStayLogged);
        email = (EditText) view.findViewById(R.id.editTextEmail);
        password = (EditText) view.findViewById(R.id.editTextPassword);


        //Set toolbar title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Přihlášení");

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set edittexts text
        email.setText(sharedPreferences.getString("email", ""));
        password.setText(sharedPreferences.getString("password", ""));

        //validator will check errors
        validator = new Validator(this);
        validator.setValidationListener(this);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start validation
                validator.validate();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show RegisterFragment
                showFragment(RegisterFragment.newInstance(), RegisterFragment.TAG, true);
            }
        });
    }

    //Fragment's newInstance method
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onValidationSucceeded() {
        //get datas from edittexts
        emailString = email.getText().toString();
        passwordString = password.getText().toString();

        //Put datas to parameters
        RequestParams params = new RequestParams();
        params.put("password", passwordString);
        params.put("email", emailString);

        MyHttpClient.post("login", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

                Snackbar.make(view, "Chyba", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Gson gson = new Gson();

                //save datas from JSON to User.class
                user = gson.fromJson(response.toString(), User.class);


                //Check if user wants to save his login
                if (stayLogged.isChecked()) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("email", emailString);
                    editor.putString("password", passwordString);
                    editor.apply();
                } else {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("email", "");
                    editor.putString("password", "");
                    editor.apply();
                }

                Snackbar.make(view, "Úspěšně přihlášen", Snackbar.LENGTH_SHORT).show();

                //Open MessagesFragment
                showFragment(MessagesFragment.newInstance(), MessagesFragment.TAG, false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
