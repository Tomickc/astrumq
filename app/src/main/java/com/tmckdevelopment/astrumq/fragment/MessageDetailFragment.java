package com.tmckdevelopment.astrumq.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tmckdevelopment.astrumq.MainActivity;
import com.tmckdevelopment.astrumq.R;

/**
 * Created by Tomáš Vítek on 12.05.2016 at 19:06.
 */
public class MessageDetailFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "message_detail_fragment";

    //Define Views
    TextView email, messagesCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        //Set actionBar text as nothing
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Podrobnosti");
        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back when arrow is clicked
                getFragmentManager().popBackStack();
            }
        });

        //Declare Views
        email = (TextView) view.findViewById(R.id.textViewEmailDetail);
        messagesCount = (TextView) view.findViewById(R.id.textViewMessagesCount);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        email.setText(((MainActivity) getActivity()).author.email);
        messagesCount.setText(((MainActivity) getActivity()).author.commentsCount);

        //On email click open email app and write email
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{((MainActivity) getActivity()).author.email});
                try {
                    startActivity(Intent.createChooser(i, "Odeslat z"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Nejsou nainstalováný žádné emailové programy", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    //Fragment's newInstance method
    public static MessageDetailFragment newInstance() {
        MessageDetailFragment fragment = new MessageDetailFragment();
        return fragment;
    }

}
